# Podracer Cards

![Preview of the podracer themed cards](./cards-preview.jpg)

Podracer themed card variant for Martin Racing Federation.
See on [Gaslands.com](https://gaslands.com/martian-racing-federation/)

## Content

### Single Cars

Every Card rendered as a 750x1050px png

### PDF

A pdf ready to print on a normal A4 paper is included. The last page contains the card backs.

### SVG

A inkscape svg file with the design of the cards

#### Fonts

Because I don't know if I can include the font files in this repository, you will need to download the following fonts for displaying and editing the svg cards.

* [Aurebesh from aurebesh.org](https://aurebesh.org/font/Aurebesh.otf)
* [Nal-Huttese font from oocities.org](https://www.oocities.org/timessquare/4965/sffont.html?20228#huttese)
